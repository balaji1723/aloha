from django.db import models
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel


class State(models.Model):
    """
    state model
    """

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "States"


class Student(models.Model):
    """
        Student model
    """

    name = models.CharField(max_length=255)
    hall_ticket = models.CharField(max_length=10, unique=True)
    state = models.ForeignKey(State, related_name="students", null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Students"


class Marks(models.Model):
    """
        Marks model
    """

    student = models.OneToOneField(Student, related_name="marks", on_delete=models.CASCADE, unique=True)
    python = models.IntegerField()
    web_development = models.IntegerField()
    django = models.IntegerField()

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = "Marks"
