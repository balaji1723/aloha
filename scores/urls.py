from django.urls import include, path
from rest_framework import routers
from scores import views

app_name = "scores"

router = routers.DefaultRouter()
router.register(r'states', views.StateViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'students', views.StudentViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]