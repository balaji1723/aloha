from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from scores.serializers import UserSerializer, GroupSerializer, StateSerializer, StudentSerializer
from scores.models import State, Student


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class StateViewSet(viewsets.ModelViewSet):

    queryset = State.objects.all()
    serializer_class = StateSerializer


class StudentViewSet(viewsets.ModelViewSet):

    queryset = Student.objects.all()
    serializer_class = StudentSerializer


