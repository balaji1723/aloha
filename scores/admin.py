from django.contrib import admin
from scores.models import State, Marks, Student


class StateAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_per_page = 10


class MarksAdmin(admin.ModelAdmin):
    list_display = ('student', 'python', 'web_development', 'django')
    list_filter = ('student',)
    raw_id_fields = ['student', ]
    list_per_page = 10


class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'hall_ticket', 'state')
    list_filter = ('hall_ticket', 'state')
    raw_id_fields = ['state']
    list_per_page = 10


# Register your models here.
admin.site.register(State, StateAdmin)
admin.site.register(Marks, MarksAdmin)
admin.site.register(Student, StudentAdmin)